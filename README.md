**Spring Boot demo for TGL assignment**  
---
1. designed 3-tiers (controller, service, dao)  
2. build basic CRUD features  
3. prepare mysql 5.7 and execute /src/main/resources/ employee_data.sql  
4. configure log, context path, db connection ...etc. in /src/main/resources/application.yml   
5. DO USE MyBatis 3
6. execute command - mvn spring-boot:run  
7. view swagger UI - http://localhost:8080/swagger-ui.html
8. try employee CRUD from swagger UI try-it-out