package com.tgl.mvc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.tgl.mvc.exception.EmployeeNotValidException;
import com.tgl.mvc.exception.RecordNotFoundException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.IGenericService;

public abstract class AEmployeeController {
  
  protected Logger logger;

  protected IGenericService<Employee> employeeService;
  
  @PostMapping
  public ResponseEntity<Long> insert(@RequestBody @Valid Employee employee) {
    long createdId = employeeService.insert(employee);
    return new ResponseEntity<>(createdId, HttpStatus.CREATED);
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<Boolean> delete(@PathVariable("id") @Min(1) long employeeId) {
    Employee result = employeeService.findById(employeeId);
    if (result == null) {
      throw new RecordNotFoundException(employeeId);
    }
    boolean isEffected = employeeService.delete(employeeId);
    return new ResponseEntity<>(isEffected, HttpStatus.OK);
  }

  @PutMapping
  public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {
    Employee result = employeeService.findById(employee.getId());
    if (result == null) {
      throw new RecordNotFoundException(employee.getId());
    }
    result = employeeService.update(employee);
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) long employeeId) {
    Employee employee = employeeService.findById(employeeId);
    if (employee == null) {
      throw new RecordNotFoundException(employeeId);
    }
    return new ResponseEntity<>(employee, HttpStatus.OK);
  }

  @PostMapping("/batchInsert")
  public boolean batchInsert(@RequestParam("file") MultipartFile file)
      throws EmployeeNotValidException {

    if (file == null || file.isEmpty()) {
      return false;
    }

    List<String> rawList = new ArrayList<>();
    try (BufferedReader bfReader =
        new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
      String line = null;
      while ((line = bfReader.readLine()) != null) {
        rawList.add(line);
      }
    } catch (IOException e) {
      logger.error("file: {}, error: {}", file.getName(), e);
    }

    return employeeService.batchInsert(rawList);
  }
}
