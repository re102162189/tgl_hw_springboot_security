package com.tgl.mvc.controller;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.IGenericService;

/**
 * 
 * @author kite.chen Designed for Employee Controller
 */
@RestController
@RequestMapping(value = "/rest/employee/mybatis")
public class EmployeeMyBatisController extends AEmployeeController {
  
  @Autowired
  public EmployeeMyBatisController(@Qualifier("employeeMyBatisService") IGenericService<Employee> employeeService) {
    this.employeeService = employeeService;
    this.logger = LogManager.getLogger(EmployeeMyBatisController.class);
  }
}
