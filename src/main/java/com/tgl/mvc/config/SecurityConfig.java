package com.tgl.mvc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  
  @Value("${write.username}")
  private String writeUsername;

  @Value("${write.password}")
  private String writePassword;
  
  @Value("${write.role}")
  private String writeRole;

  @Value("${read.username}")
  private String readUsername;

  @Value("${read.password}")
  private String readPassword;
  
  @Value("${read.role}")
  private String readRole;
  
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
         .withUser(readUsername).password(encoder().encode(readPassword)).roles(readRole).and()
         .withUser(writeUsername).password(encoder().encode(writePassword)).roles(writeRole);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .httpBasic().and()
      .authorizeRequests()
        .antMatchers(HttpMethod.GET, "/rest/**").hasAnyRole(readRole, writeRole)
        .antMatchers(HttpMethod.POST, "/rest/**").hasRole(writeRole)
        .antMatchers(HttpMethod.PUT, "/rest/**").hasRole(writeRole)
        .antMatchers(HttpMethod.DELETE, "/rest/**").hasRole(writeRole)
        .and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .sessionFixation().migrateSession().and()
        .csrf().disable().cors().disable();
  }

  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder();

  }
}
