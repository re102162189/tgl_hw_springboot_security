package com.tgl.mvc.exception;

public class EmployeeNotValidException extends Exception {

  private static final long serialVersionUID = 5863888900559217046L;

  public EmployeeNotValidException(String message) {
    super(message);
  }
}
