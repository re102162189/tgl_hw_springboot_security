package com.tgl.mvc.service;

import java.util.List;
import com.tgl.mvc.exception.EmployeeNotValidException;
import com.tgl.mvc.model.Employee;

public interface IGenericService <T> {
  
  T findById(long id);
  
  long insert(T t);
  
  Employee update(T t);
  
  boolean delete(long id);
  
  boolean batchInsert(List<String> list) throws EmployeeNotValidException;
}
