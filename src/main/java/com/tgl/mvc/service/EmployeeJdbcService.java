package com.tgl.mvc.service;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgl.mvc.dao.IGenericDao;
import com.tgl.mvc.model.Employee;

/**
 * 
 * @author kite.chen Designed for business logic
 *
 */
@Service
@Qualifier("employeeJdbcService")
public class EmployeeJdbcService extends AEmpolyeeService implements IGenericService<Employee> {
  @Autowired
  public EmployeeJdbcService(@Qualifier("employeeJdbcDao") IGenericDao<Employee> employeeDao) {
    this.employeeDao = employeeDao;
    this.logger = LogManager.getLogger(EmployeeMyBatisService.class);
  }
}
