package com.tgl.mvc.dao;

import java.sql.SQLException;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgl.mvc.mapper.EmployeeRowMapper;
import com.tgl.mvc.model.Employee;

@Repository
@Qualifier("employeeJdbcDao")
public class EmployeeJdbcDao implements IGenericDao<Employee> {

  private static final Logger LOG = LogManager.getLogger(EmployeeJdbcDao.class);

  private static final String INSERT =
      "INSERT INTO employee (ch_name, eng_name, email, height, weight, bmi, phone) VALUES (:chName, :engName, :email, :height, :weight, :bmi, :phone)";

  private static final String DELETE = "DELETE FROM employee WHERE id=:id";

  private static final String UPDATE =
      "UPDATE employee SET ch_name=:chName, eng_name=:engName, email=:email, height=:height, weight=:weight, bmi=:bmi, phone=:phone WHERE id=:id";

  private static final String SELECT =
      "SELECT id, ch_name, eng_name, email, height, weight, bmi, phone FROM employee";

  @Autowired
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  public long insert(Employee employee) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(employee),
        keyHolder);
    return keyHolder.getKey().longValue();
  }

  @CacheEvict(value = "employeeFindById", key = "#employeeId")
  public int delete(long employeeId) {
    return namedParameterJdbcTemplate.update(DELETE, new MapSqlParameterSource("id", employeeId));
  }

  @CachePut(value = "employeeFindById", key = "#employee.id")
  public int update(Employee employee) {
    return namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(employee));
  }

  @Cacheable(value = "employeeFindById", key = "#employeeId")
  public Employee findById(long employeeId) {
    try {
      return namedParameterJdbcTemplate.queryForObject(SELECT + " WHERE id=:id",
          new MapSqlParameterSource("id", employeeId), new EmployeeRowMapper());
    } catch (EmptyResultDataAccessException e) {
      LOG.error("EmployeeDao findById failed, id: {}, Exception: {}", employeeId, e);
    }
    return null;
  }

  @CacheEvict(value = "employeeFindById")
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
  public boolean batchInsert(List<Employee> list) {
    SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(list.toArray());
    int[] updateCounts = namedParameterJdbcTemplate.batchUpdate(INSERT, batch);
    return updateCounts.length > 0;
  }
}
