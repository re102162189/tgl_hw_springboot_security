package com.tgl.mvc.dao;

import java.util.List;
import com.tgl.mvc.exception.EmployeeNotValidException;

public interface IGenericDao<T>  {
  
  T findById(long id);
  
  long insert(T t);
  
  int update(T t);
  
  int delete(long id);
  
  boolean batchInsert(List<T> list) throws EmployeeNotValidException;
  
}
