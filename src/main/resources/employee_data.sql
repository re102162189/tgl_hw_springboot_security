# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.27)
# Database: test
# Generation Time: 2020-03-30 08:54:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table employee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ch_name` varchar(32) NOT NULL DEFAULT '',
  `eng_name` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(32) NOT NULL DEFAULT '',
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `bmi` double DEFAULT NULL,
  `phone` varchar(4) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;

INSERT INTO `employee` (`id`, `ch_name`, `eng_name`, `email`, `height`, `weight`, `bmi`, `phone`, `create_time`, `update_time`)
VALUES
	(1,'廖強原','Edward-Liaw','edwardliaw@transglobe.com.tw',181,64,19.53542321662953,'6120','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(2,'黃智原','Aaron-Huang','AaronHuang@transglobe.com.tw',177,80,25.53544639152223,'2115','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(3,'黃大羚','Aya-Wong','ayawong@transglobe.com.tw',154,69,29.094282341035587,'6144','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(4,'陳美鴻','Alvin-Chen','AlvinChen@transglobe.com.tw',163,53,19.94805976890361,'6121','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(5,'洪毅芳','Amy-Hung','AmyHung@transglobe.com.tw',182,88,26.56683975365294,'6190','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(6,'王雯妮','Annie-Wang','AnnieWang@transglobe.com.tw',160,88,34.37499999999999,'6159','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(7,'薛安承','Cosmo-Hsueh','CosmoHsueh@transglobe.com.tw',189,69,19.31636852271773,'6140','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(8,'陳祐森','Disen-Chen','disenchen@transglobe.com.tw',175,66,21.551020408163264,'6139','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(9,'張迪道','Douglas-Chang','DouglasChang@transglobe.com.tw',151,58,25.437480812245077,'6119','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(10,'高守緯','George-Gao','GeorgeGao@transglobe.com.tw',165,56,20.569329660238754,'6118','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(11,'張至豪','Howard-Chang','HowardChang@transglobe.com.tw',154,71,29.93759487265981,'6147','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(12,'劉哲航','James-Liu','JamesLiu@transglobe.com.tw',174,75,24.772096710265558,'6143','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(13,'王嘉銘','Jeffrey-Wang','JeffreyWang@transglobe.com.tw',186,71,20.522603769221874,'6127','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(14,'鄭潘毅','Jeremy-Cheng','jeremycheng@transglobe.com.tw',181,55,16.788254326791,'6174','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(15,'楊唯霖','Jet-Yang','jetyang@transglobe.com.tw',153,75,32.038959374599514,'2113','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(16,'王杰凱','Jobim-Wang','jobimwang@transglobe.com.tw',185,58,16.946676406135865,'6180','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(17,'沈揚妤','Kate-Shen','KateShen@transglobe.com.tw',185,85,24.835646457268076,'6137','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(18,'施婕秀','Linda-Shih','lindashih@transglobe.com.tw',187,52,14.870313706425689,'2111','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(19,'陳孟汝','Maggie-Chen','maggiechen@transglobe.com.tw',170,85,29.411764705882355,'6127','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(20,'余玫澤','Martin-Yu','martinyu@transglobe.com.tw',161,73,28.162493730951734,'6148','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(21,'楊承銘','Obastan-Yang','obastanyang@transglobe.com.tw',187,52,14.870313706425689,'6133','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(22,'熊文芸','Phyllis-Hsiung','PhyllisHsiung@transglobe.com.tw',189,72,20.15621063240111,'6122','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(23,'張若宇','Shaoyu-Chang','ShaoyuChang@transglobe.com.tw',178,50,15.780835753061481,'6157','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(24,'符少玲','Shirley-Fu','shirleyfu@transglobe.com.tw',183,54,16.124697661918837,'6129','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(25,'劉聖緯','Steve-Liu','SteveLiu@transglobe.com.tw',187,72,20.58966513197403,'6123','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(26,'謝俊燕','Yetta-Shieh','yettashieh@transglobe.com.tw',176,84,27.117768595041323,'6126','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(27,'林青承','YuCheng-Lin','YuChengLin@transglobe.com.tw',169,87,30.461118308182492,'6128','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(28,'張昱琳','Christine-Chang','christinechang@transglobe.com.tw',183,54,16.124697661918837,'6157','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(29,'蔡琬炯','Akino-Tsai','akinotsai@transglobe.com.twa',187,72,20.58966513197403,'6106','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(30,'嚴銘莉','Amery-Yen','ameryyen@transglobe.com.tw',176,84,27.117768595041323,'6141','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(31,'張清興','Arthur-Chang','arthurchang@transglobe.com.tw',169,87,30.461118308182492,'6164','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(32,'游永瓏','Benson-Yu','BensonYu@transglobe.com.tw',151,79,34.64760317529933,'2155','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(33,'蘇輝慧','Chiahui-Su','ChiahuiSu@transglobe.com.tw',176,66,21.306818181818183,'6177','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(34,'盧佳莉','Chili-Lu','chililu@transglobe.com.tw',169,66,23.1084345786212,'6177','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(35,'徐琪銘','Colin-Hsu','ColinHsu@transglobe.com.tw',178,58,18.30576947355132,'6121','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(36,'王志皓','Curious-Wang','CuriousWang@transglobe.com.tw',160,58,22.656249999999996,'6174','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(37,'林祺娟','Dania-Lin','danialin@transglobe.com.tw',186,57,16.475893166840095,'6162','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(38,'周秀華','Daniel-Chou','danielchou@transglobe.com.tw',161,67,25.847768218818715,'6153','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(39,'陳正祺','David-Chen','davidchen@transglobe.com.tw',171,89,30.436715570602924,'6109','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(40,'覃戴誠','David-Chin','DavidChin@transglobe.com.tw',188,66,18.673607967406067,'6187','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(41,'孔智維','David-Kung','DavidKung@transglobe.com.tw',152,69,29.864958448753463,'6141','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(42,'王大迪','Dean-Wang','DeanWang@transglobe.com.tw',183,53,15.826092149661081,'6173','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(43,'潘俊慧','Debby-Pan','debbypan@transglobe.com.tw',168,50,17.71541950113379,'6137','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(44,'辛麗達','Eddy-Shin','EddyShin@transglobe.com.tw',189,73,20.43615800229557,'6184','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(45,'吳易珍','Ella-Wu','ellawu@transglobe.com.tw',160,53,20.703124999999996,'6124','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(46,'程佩恆','Eric-Cheng','ericcheng@transglobe.com.tw',176,61,19.692665289256198,'6152','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(47,'林福安','Eric-Lin','ericlinja@transglobe.com.tw',171,57,19.493177387914233,'6163','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(48,'林俊宏','Frank-Lin','FrankLin@transglobe.com.tw',170,55,19.031141868512112,'6168','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(49,'陳宗豪','Freddy-Chen','FreddyChen@transglobe.com.tw',162,76,28.959000152415786,'6191','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(50,'張文維','Henry-Chang','HenryChang@transglobe.com.tw',189,83,23.23563170124017,'6135','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(51,'潘宏貞','Hope-Pan','hopepan@transglobe.com.tw',166,53,19.233560749020178,'6134','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(52,'許惠輝','Jack-Hsu','jackhsu@transglobe.com.tw',162,70,26.672763298277697,'6160','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(53,'涂良敏','Jack-Tu','jacktu@transglobe.com.tw',164,71,26.397977394408095,'6119','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(54,'吳鏡民','James-Wu','jameswu@transglobe.com.tw',185,73,21.32943754565376,'6107','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(55,'徐健杰','Jay-Hsu','JayHsu@transglobe.com.tw',171,58,19.835162956123252,'6183','2020-03-30 08:46:32','2020-03-30 08:46:32'),
	(56,'羅民','Jay-Lo','jaylo@transglobe.com.tw',156,62,25.476660092044707,'6997','2020-03-30 08:46:32','2020-03-30 08:46:32');

/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
